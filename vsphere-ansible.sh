#!/bin/bash

set -ex

# Check if running as sudo
if [[ $EUID > 0 ]]; then 
  echo "Please run as sudo"
  exit 1
fi


source_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )

rm -rf /tmp/vsphere-lab-deploy

cd /tmp

git clone https://github.com/yasensim/vsphere-lab-deploy.git

cp -a $source_dir/vsphere-nsx-lab-deploy-mod/* vsphere-lab-deploy

cd vsphere-lab-deploy

ansible-playbook deploy.yml

