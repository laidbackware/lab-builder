# lab-builder

Project to build a home lab with vSphere 6.5, NSX-T 2.4 and PKS.

**Forket Projects:**
vSphere build using - https://github.com/yasensim/vsphere-lab-deploy
NSX-T build using - https://github.com/vmware/ansible-for-nsxt

**TODO**
vSphere
* Allow ESXi PW changes
* Upgrade to 6.7
* Make ESXi boot check dynamic
* Prevent error on vCenter already built

    
NSX-T
* Allow Edge build using nice names for vCenter objects
* Switching and routing linking
* Host transport node config

